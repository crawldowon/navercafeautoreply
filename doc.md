콘솔창 제거 -w

pyinstaller -w 파이썬파일
<!-- ex) pyinstaller -w --noconfirm main.py -->
pyinstaller --onefile --noconfirm --windowed main.py

<!-- pyinstaller -w --noconfirm --onefile --add-binary "chromedriver.exe";"." .py -->
pyinstaller ./main.py --onefile --noconsole --add-binary "./driver/chromedriver.exe;./driver"
